# YKinseason

This is a collection of analytical tools, written in R, intended to aid in-season analysis of Yukon river chinook and chum.

## Disclaimer

*This package is under development. Functions will change rapidly and substantially.
Do not use these if you are not part of the development team!*


## Installation

To install this package directly from gitlab you will need to have the R package `remotes` installed. 

```
install.packages("remotes") # Install the remotes package

url.use <- "https://gitlab.com/MichaelFolkes/ykinseason.git" 

remotes::install_git(url = url.use)
``` 

## Application

The [`dev`](https://gitlab.com/MichaelFolkes/ykinseason/-/tree/master/dev) folder in this repository has some example R scripts (not too user friendly) that show how the package can be applied.  These need to be downloaded from gitlab as they do not play a part in the installation process. In particular, downloading the files `Pilot_Sonar_In-season_Projections.rmd` and `yukon_runsize_timing.R` might be the most useful.

The example scripts are set to find data files based on a specific folder structure.  To work 'out of the box' it would be easiser to have the layout shown below. The quick summary is a parent folder (e.g. runsize_inseason) in that are the R and data folders. In the data folder is pilot and eagle. So the R session start in the R folder, this is the home directory. Based on that, scripts look for data by looking "../data/pilot" or "../data/eagle".

``` 
   
+-runsize_inseason
   |   .Rhistory
   |   test.txt
   |   Yukon Chinook run timing_ADFGmethod.xlsx
   | 
   +---data
   |   +---eagle
   |   |       .Rhistory
   |   |       2020-07-05Passage Tables.html
   |   |       2020-07-06Passage Tables.html
   |   |       2020-07-07Passage Tables.html
   |   |       2020-07-08Passage Tables.html
   |   |       2020-07-09Passage Tables.html
   |   |       2020-07-10Passage Tables.html
   |   |       2020-07-11Passage Tables.html
   |   |       2020-07-12Passage Tables.html
   |   |       2020-07-13Passage Tables.html
   |   |       2020-07-14Passage Tables.html
   |   |       2020-07-15Passage Tables.html
   |   |       2020-07-16Passage Tables.html
   |   |       Master Eagle Sonar Multiyear Chinook Count Summary 2005 - 2019.xlsx
   |   |       
   |   \---pilot
   |           .Rhistory
   |           2020-06-08Passage Tables.html
   |           2020-06-09Passage Tables.html
   |           2020-06-10Passage Tables.html
   |           2020-06-11Passage Tables.html
   |           2020-06-12Passage Tables.html
   |           2020-06-13Passage Tables.html
   |           2020-06-14Passage Tables.html
   |           2020-06-15Passage Tables.html
   |           2020-06-16Passage Tables.html
   |           2020-06-17Passage Tables.html
   |           2020-06-18Passage Tables.html
   |           2020-06-19Passage Tables.html
   |           2020-06-20Passage Tables.html
   |           2020-06-21Passage Tables.html
   |           2020-06-22Passage Tables.html
   |           2020-06-23Passage Tables.html
   |           2020-06-24Passage Tables.html
   |           2020-06-25Passage Tables.html
   |           2020-06-26Passage Tables.html
   |           2020-06-27Passage Tables.html
   |           2020-06-28Passage Tables.html
   |           2020-06-29Passage Tables.html
   |           2020-06-30Passage Tables.html
   |           2020-07-01Passage Tables.html
   |           2020-07-02Passage Tables.html
   |           2020-07-03Passage Tables.html
   |           2020-07-04Passage Tables.html
   |           2020-07-05Passage Tables.html
   |           2020-07-06Passage Tables.html
   |           2020-07-07Passage Tables.html
   |           2020-07-08Passage Tables.html
   |           2020-07-09Passage Tables.html
   |           2020-07-10Passage Tables.html
   |           2020-07-11Passage Tables.html
   |           2020-07-12Passage Tables.html
   |           2020-07-13Passage Tables.html
   |           2020-07-14Passage Tables.html
   |           2020-07-15Passage Tables.html
   |           2020-07-16Passage Tables.html
   |           2020-07-17Passage Tables.html
   |           
   \---R
           gsi_bootstrap.R
           Pilot_Sonar_In-season_Projections.rmd
           readMultiyear_files.R
           yukon_runsize_timing.R
   
        
```