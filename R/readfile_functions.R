
readCumulativeCounts <- function(filepaths){
  #this is an archived function
  station.names <- readxl::read_xlsx(path = filepaths[1], sheet = "Historical Data", n_max =  1, col_names = FALSE)
  station.names <- unlist(station.names[1,])
  names(station.names) <- NULL
  station.names <- zoo::na.locf(station.names)

  years <- readxl::read_xlsx(path = filepaths[1], sheet = "Historical Data", range = "c3:cj3", col_names = FALSE)
  years <- unlist(years[1,])
  names(years) <- NULL

  counts.cumul <- readxl::read_xlsx(path = filepaths[1], sheet = "Historical Data", range = "b4:cj84", col_names = FALSE)
  colnames.val <- paste0(years,station.names)
  colnames.val <- c("date.tmp", colnames.val)
  colnames(counts.cumul) <- colnames.val

  #temporarily put zeros in NA cells so diff works properly
  counts.cumul.tmp <- counts.cumul
  counts.cumul.tmp[is.na(counts.cumul.tmp)] <- 0
  counts <- as.data.frame(apply(counts.cumul.tmp[,2:ncol(counts.cumul.tmp)], 2, diff))

  #diff loses the first line so add back in:
  counts <- rbind(counts.cumul.tmp[1,2:ncol(counts.cumul.tmp)], counts)
  counts$date.tmp <- counts.cumul$date.tmp
  #get date column back to first column:
  counts <- counts[,c(ncol(counts), 1:(ncol(counts)-1))]

  #put NA values back in:
  counts[is.na(counts.cumul)] <- NA

  counts.long <- reshape(counts, direction = 'long', varying = list(2:ncol(counts)), timevar = "field.index", v.names = "counts")
  counts.long$year_station <- attributes(counts.long)$reshapeLong$varying[[1]][counts.long$field.index]
  counts.long$year <- substr(counts.long$year_station,1,4)
  counts.long$staton <- substr(counts.long$year_station,5,nchar(counts.long$year_station))

  counts.long$date <- paste(counts.long$year, format(counts.long$date.tmp, "%m-%d"), sep = "-")

  counts.long$od <- calcOrdinalDay(counts.long$date)

}#END readCumulativeCounts


readDailyCounts <- function(filename){
  dat.tmp <- read.csv(filename, stringsAsFactors = FALSE)
  #od is ordinal day (day of year)
  dat.tmp$od <- calcOrdinalDay(paste(dat.tmp$date, dat.tmp$year, sep="-"), "%d-%b-%Y")

  data.long <- reshape(dat.tmp, direction = "long", varying = list(c("leftbank", "rightbank")), timevar = "bank", v.names="count" )
  bank.columns <- attributes(data.long)$reshapeLong$varying[[1]]
  bank.columns <- sub(pattern = "bank", replacement = "", x = bank.columns)
  data.long$bank <- bank.columns[data.long$bank]
  data.long$count <- gsub(pattern = ",", "", data.long$count)
  data.long$count <- as.integer(data.long$count)
  data.long <- subset(data.long, select = -id)
  data.wide <- reshape(data.long, direction = "wide", timevar = "bank", idvar = c("system", "year", "date", "od", "comments" ))

  return(list(data.wide=data.wide, data.long=data.long))

}#END readDailyCounts


.readEagleHTML <- function(filename){
  #require(XML)

  tables <- XML::readHTMLTable(html.filenames.current, stringsAsFactors = FALSE)
  sonar.current.year <- tables[[3]]

  rows.exclude <- c("Total", "Var", "se", "cv(percent)", "L90", "U90")
  sonar.current.year <- sonar.current.year[! sonar.current.year$Date %in% rows.exclude, ]

  #next removes the 1000's comma in counts:
  sonar.current.year[,-1] <- apply(sonar.current.year[,-1],2, function(x) as.integer(gsub(",", "", x)))
  sonar.current.year$od <- calcOrdinalDay(sonar.current.year$Date, format = "%m/%d/%Y")
  sonar.current.year.total <- sum(sonar.current.year$TOTAL)

  sonar.current.year$date <- as.Date(calcDate(sonar.current.year$od))

  return(list(sonar.current.year=sonar.current.year, tables=tables))

}#END readEagleHTML



readGSIchum_adfg <- function(filenames){


  files.list <- lapply(filenames, function(filename.single){

    sheets.names <- excel_sheets(filename.single)
    #only grab sheets named 'stratum' or "summer' or 'fall'
   #browser()
    sheets.names <- sheets.names[grep("Stratum|Summer|Fall", x = sheets.names)]
    #exclude combined strata sheets:
    if(length(grep("\\&", x = sheets.names))>0) {sheets.names <- sheets.names[-grep("\\&", x = sheets.names)]}


    sheets.list <- lapply(sheets.names, function(sheets.names.single){

      dat.row1 <- read_excel(filename.single, sheet = sheets.names.single, n_max = 1, col_names = FALSE)
      dat.tmp <- read_excel(filename.single, sheet = sheets.names.single, range = "a2:e22", col_names = TRUE)
    dat.tmp <- dat.tmp[!is.na(dat.tmp$GROUP),]
    colnames(dat.tmp)[4:5] <- c("l95", "u95")
    colnames(dat.tmp) <- tolower(colnames(dat.tmp))
    cat(paste("\n",filename.single, "\n", sheets.names.single, "\n"))
    #browser()
    if(length(dat.row1$...3)>0){
      samplesize <- as.integer(trimws(unlist(strsplit(dat.row1$...3, split = "="))[2]))
    }else{
        samplesize <- NA
      }

    dat.row1 <- trimws(unlist(strsplit(dat.row1$...1, split = "=")))
    stratum <- dat.row1[1]
   #browser()
    dates <- trimws(unlist(strsplit(dat.row1[2], "-|to")))
    filename.single <- basename(tools::file_path_sans_ext(filename.single))
    char.n <- nchar(filename.single)
    year <- substr(filename.single, char.n-3, char.n)
    dates <- as.Date(paste(year, dates, sep="/"))
    date.start <- dates[1]
    date.end <- dates[2]


   # browser()
    if(dat.tmp$group[5]=="Tanana") dat.tmp$group[5] <- "TananaSummer"
    if(dat.tmp$group[7]=="Tanana") dat.tmp$group[7] <- "TananaFall"

    dat.tmp$stratum <- stratum
    dat.tmp$date.start <- date.start
    dat.tmp$date.end <- date.end
    row.names(dat.tmp) <- NULL

    return(list(filename=filename.single, sheet=sheets.names.single, stratum=stratum, dates=dates, samplesize=samplesize,  data.gsi=dat.tmp ))

      })

    names(sheets.list) <- sheets.names

    data.gsi <- lapply(sheets.list, "[[", "data.gsi")
    data.gsi.combined <- do.call("rbind", data.gsi)
    row.names(data.gsi.combined) <- NULL

    return(list(sheets=sheets.list, data.gsi.combined=data.gsi.combined))


  })
  names(files.list) <- basename(filenames)

  data.gsi.combined <- data.gsi <- lapply(files.list, "[[", "data.gsi.combined")
  data.gsi.combined <- do.call("rbind", data.gsi.combined)
  row.names(data.gsi.combined) <- NULL

  return(list(data.gsi.combined=data.gsi.combined, files.list=files.list))

}#END readGSIchum_adfg




readGSIstrata <- function(filename){

 # require(readxl)
  dat <- as.data.frame(readxl::read_excel(filename, skip = 1))
  colnames(dat) <- tolower(colnames(dat))

  #discard average rows at end
  rows.average <- grep(pattern = "Average", dat$year)
  dat <- dat[1:(rows.average-1),]

  #keep stratum rows:
  rows.keep <- grep(pattern = "Stratum", dat$strata)
  dat <- dat[rows.keep,1:7]
  #fill empty year cells:
  dat$year <- repeat.before(dat$year)
  dat$stratum <- substr(dat$strata,9,9)

  dat$date.start <- substr(dat$dates,1,5)
  dat$date.start <- as.Date(paste(dat$year, dat$date.start, sep="/"))
  dat$date.end <- substr(dat$dates,nchar(dat$dates[1])-5,nchar(dat$dates[1]))
  dat$date.end <- as.Date(paste(dat$year, dat$date.end, sep="/"))

  colnames(dat)[5:7] <- c("run.prop", "prop.cdn", "passage.cdn")
  dat <- subset(dat, select=-strata)

  dat[,1:7] <- type.convert(dat[,1:7])
  return(dat)

}#END readGSIstrata


readGSIuncertainty <- function(filename){
  #require(readxl)
  dat <- as.data.frame(readxl::read_excel(filename, skip = 2))
  colnames(dat) <- tolower(colnames(dat))

  dat <- dat[,c(1:4, 6:8,10:12, 14:16)]

  strata <- rep(1:4,rep(3,4))
  statistic <- rep(c("mean", "l90", "u90"),4)
  colnames(dat)[2:ncol(dat)] <- paste0("stratum", strata, "_", statistic )
  data.long <- reshape(dat, direction = "long", list(2:ncol(dat)), v.names = "value")
  data.long$stratum <- strata[data.long$time]
  data.long$statistic <- statistic[data.long$time]
  data.long <- data.long[,c("year", "stratum", "statistic", 'value')]

  data.long2 <- reshape(data.long, direction = 'wide', idvar = c('year', "stratum"), timevar = "statistic")

  return(list(data.wide=dat, data.long=data.long, data.long2=data.long2))


}#END readGSIuncertainty

readPilotregresssionXLS <- function(filename, range="A4:AC25"){

  #require(readxl)

  dat <- as.data.frame(readxl::read_excel(filename, range=range))
  dat <- dat[-1,]
  colnames(dat) <- tolower(colnames(dat))
  dat.runsize <- dat[,1:2]
  dat <- subset(dat, select = -tr)
  colnames(dat)[2:ncol(dat)] <- seq.Date(as.Date("2020-6-15"), by = "1 day", length.out = ncol(dat)-1)
  dat.long <- reshape(dat, direction = 'long', varying = list(2:ncol(dat)), timevar = "date", v.names = "counts")
  dat.long$date <- as.integer(attributes(dat.long)$reshapeLong$varying[[1]][dat.long$date])
  dat.long$date <- as.Date(dat.long$date, origin = "1970-01-01" )
  dat.long$date <- as.Date(paste(dat.long$year, format(dat.long$date, "%B"), format(dat.long$date, "%d"), sep = "-"), "%Y-%B-%d")
  dat.long$date.noleap <- as.Date(paste(2019, format(dat.long$date, "%B"), format(dat.long$date, "%d"), sep = "-"), "%Y-%B-%d")

  dat.long <- merge(dat.long, dat.runsize)


  return(list(data.runsize=dat.runsize, data.wide=dat, data.long=dat.long))

}#END readPilotregresssionXLS


readPilotGSI <- function(filename){
  #require(readxl)
  comments<- unlist(readxl::read_excel(filename, sheet = "Estimates", range = "A1", col_names = FALSE))

  dat <- as.data.frame(readxl::read_excel(filename, sheet = "Estimates", skip = 2))
  stratum.na <- is.na(dat$Stratum)
  #dates <- dat$Stratum[! stratum.na]
  dat$Stratum <- repeat.before(dat$Stratum)
  date1.index.start <- unlist(gregexpr(pattern = "\\(", text = dat$Stratum))+1
  date1.index.end <- unlist(gregexpr(pattern = "-", text = dat$Stratum))-1
  date2.index.start <- unlist(gregexpr(pattern = "-", text = dat$Stratum))+1
  date2.index.end <- unlist(gregexpr(pattern = "\\)", text = dat$Stratum))-1
  stratum.date.start <- substring(dat$Stratum, date1.index.start,  date1.index.end)
  dat$stratumdatestart <- as.Date(paste(format(Sys.Date(), "%Y"), stratum.date.start, sep="/" ))
  stratum.date.end <- substring(dat$Stratum, date2.index.start,  date2.index.end)
  dat$stratumdateend <- as.Date(paste(format(Sys.Date(), "%Y"), stratum.date.end, sep="/" ))

  return(list(comment=comment, data=dat))

}#END readPilotGSI



.readPilotHTML <- function(filename){
  #require(XML)

  tables <- XML::readHTMLTable(filename, stringsAsFactors = FALSE)
  sonar.current.year <- tables$`Daily Passage By Species`

  #remove commas in numbers:
  sonar.current.year[,-1] <- apply(sonar.current.year[,-1],2, function(x) as.integer(gsub(",", "", x)))

  rows.stats <- c("Total", "Var", "se", "cv(percent)", "L90", "U90")
  sonar.stats <- sonar.current.year[ sonar.current.year$Date %in% rows.stats,]
  colnames(sonar.stats)[1] <- "statistic"

  sonar.stats$statistic <- gsub(pattern = "\\(|\\)", replacement = "",sonar.stats$statistic)

  sonar.current.year <- sonar.current.year[! sonar.current.year$Date %in% rows.stats, ]

  sonar.current.year$od <- calcOrdinalDay(sonar.current.year$Date, format = "%m/%d/%Y")
  sonar.current.year.total <- sum(sonar.current.year$ALLCHINOOK)

  sonar.current.year$date <- as.Date(calcDate(sonar.current.year$od))

  return(list(tables=tables, sonar.current.year=sonar.current.year, sonar.stats=sonar.stats))

}#END readPilotHTML



readInseasonForecast <- function(filename,range="a8:y100"){

  #this is the get the second column in the range:
  colon <- regexpr(pattern = ":", range)
  range2 <- substr(x = range, colon+1, nchar(range))
  column.index <- unlist(gregexpr(pattern = "[a-zA-Z]", text = range2))
  finalCol <- substr(x = range2, start = column.index[1],stop = column.index[length(column.index)])

  colnames.val <- readxl::read_xlsx(path = filename, sheet = "Data", range= paste0("a4:", finalCol, "4"), col_names = FALSE)

  colnames.val <- unlist(colnames.val)
  names(colnames.val) <- NULL
  colnames.val[1] <- "date.tmp"
  colnames.val <- gsub(pattern = " ", replacement = "", colnames.val, fixed = TRUE)

  data.inseason <- readxl::read_xlsx(path = filename, sheet = "Data", range= range, col_names = FALSE)
  #data.inseason in a tibble - I don't like tibbles
  data.inseason <- as.data.frame(data.inseason)

  #getting rid of empty columns
  data.inseason <- data.inseason[,!is.na(colnames.val),]
  colnames.val <- colnames.val[!is.na(colnames.val)]
  colnames(data.inseason) <- colnames.val

  data.inseason$year <- as.integer(format(Sys.Date(), "%Y"))
  data.inseason$date <- as.Date(paste(data.inseason$year, format(data.inseason$date.tmp, "%m-%d"), sep = "-"))
  data.inseason$od <- calcOrdinalDay(data.inseason$date)
#browser()
  #make copies of genetic columns
  genetic.cols <- grep(pattern = "genetic", x = colnames(data.inseason), ignore.case = TRUE)
  gsi.colnames <- colnames(data.inseason)[genetic.cols]
  gsi.colnames <- gsub(pattern = "genetic-", "propCdn",gsi.colnames, ignore.case = TRUE)
  data.inseason[,gsi.colnames] <- data.inseason[,genetic.cols]/100
  data.inseason$counts.pilot <- data.inseason$`PilotDailyCount`
  data.inseason$counts.pilot.cdn <- data.inseason$counts.pilot*data.inseason$propCdnMid


  #find eagle column and make a copy:
  col.eagle <- grep(pattern = "eagle.*daily.*count", x = colnames(data.inseason), ignore.case = TRUE)

  data.inseason$counts.eagle <- data.inseason[,col.eagle]
  data.inseason$counts.eagle.l90 <- data.inseason$L90
  data.inseason$counts.eagle.u90 <- data.inseason$U90


  #exclude all imported columns (those in colnames.val):
  data.new <- data.inseason[,setdiff(names(data.inseason), colnames.val)]
  pilotCIcols <- colnames.val[grep(pattern = "Pilot.*90", x = colnames.val)]
  data.new[,pilotCIcols] <- data.inseason[,pilotCIcols]

  data.original <- data.inseason[,colnames.val]

  return(list(data= data.new, data.original=data.original))

}#END readInseasonForecast



readMultiyearCounts <- function(filename, range=NA ){

  colon <- regexpr(pattern = ":", range)
  range2 <- substr(x = range, colon+1, nchar(range))
  column.index <- unlist(gregexpr(pattern = "[a-zA-Z]", text = range2))
  finalCol <- substr(x = range2, start = column.index[1],stop = column.index[length(column.index)])

  years <- readxl::read_xlsx(path = filename, sheet = "Daily", range = paste0("b1:", finalCol, "1") , col_names = FALSE)
  years <- unlist(years[1,])
  names(years) <- NULL

  counts <- readxl::read_xlsx(path = filename , sheet = "Daily", range =range, col_names = FALSE)
  counts <- as.data.frame(counts)
  counts.long <- as.data.frame(reshape(counts, direction = 'long', varying = list(2:(length(years)+1)), timevar = "field.index", v.names = "counts"))
  #counts.long$year <- attributes(counts.long)$reshapeLong$varying[[1]][counts.long$field.index]
  counts.long$year <- years[counts.long$field.index]
  names(counts.long)[1] <- "date"

  counts.long$date <- paste(counts.long$year, format(unlist(counts.long$date), "%m-%d"), sep = "-")
  counts.long$od <- calcOrdinalDay(counts.long$date)

  counts.wide <- reshape(counts.long[,c("year",  "od", "counts")], direction = 'wide', timevar = 'year', idvar=c( 'od'))

  return(list(data.long=counts.long[,c("year", "date", "od", "counts")], data.wide=counts.wide, data.original=counts))

}#END readMultiyearCounts


readNWSrivertemp <- function(filenames){

  dat.temp <- lapply(filenames, function(x) {
    meta.header <- readLines(x)[2]
    dat.tmp <- read.csv(x, skip = 2, stringsAsFactors = FALSE)
    dat.tmp$date.str <- dat.tmp$Date
    dat.tmp$date <- strptime(dat.tmp$date.str, "%F %T")
    dat.tmp$year <- as.integer(format(dat.tmp$date, "%Y"))
    dat.tmp$month <- as.integer(format(dat.tmp$date, "%m"))
    return(list(source=meta.header, data=dat.tmp))
  })

}#readNWSrivertemp



#' Read daily ADFG html files of Pilot or Eagle results
#'
#' @param filenames A character vector. The names of html files to be imported
#'   (more than one allowed).
#' @param passagetable The name of the daily passage table in the html file (See
#'   details).
#'
#' @details The passagetable argument identifies the name of the table in the
#'   html file that holds daily passage records. This name differs between the
#'   Pilot and Eagle files.  And the Eagle table name changes by year.  These
#'   are the options, for Pilot: "Daily Passage By Species", and for Eagle:
#'   'Daily Passage By Bank 2020 Eagle Summer Season'.
#'
#' @return A list of lists. Each subordinate list represents contents of one
#'   html file. Each subordinate list includes multiple data frames.
#' @export
#'
#' @examples
#' \dontrun{
#' #current year of Pilot:
#' #html files are the pilot daily passage:
#' html.filenames <- list.files(path="../data/pilot", pattern = "Passage.*html$", full.names = TRUE)
#' #get the most recent html file:
#' html.filenames.current <- html.filenames[length(html.filenames)]
#' data.currentyear.pilot.list <- readsonarHTML(html.filenames.current)
#' data.currentyear.pilot.list <- extractsonarHTML(data.currentyear.pilot.list)
#' data.currentyear.pilot <- data.currentyear.pilot.list[[1]]$sonar.current.year
#'
#' #this example accesses the summary stats from each html file and then binds them.
#' html.filenames <- list.files(path="../data/pilot", pattern = "Passage.*html$", full.names = TRUE)
#'
#' data.currentyear.pilot.list <- readsonarHTML(html.filenames)
#' data.currentyear.pilot.list <- extractsonarHTML(data.currentyear.pilot.list)
#'
#' res <- lapply(data.currentyear.pilot.list, function(x){
#'   dat.tmp <- x$sonar.stats.wide
#'   chinook.cols.ind <- grep("ALLCHINOOK", x = colnames(dat.tmp))
#'   chinook.cols <- colnames(dat.tmp)[chinook.cols.ind]
#'   dat.tmp[,c("date", chinook.cols )]
#'   })
#'
#' res.df <- do.call("rbind", res)
#' res.df
#'
#'
#' #this reads eagle html files and grabs the daily counts and cumulative CI values
#' html.filenames <- list.files(path= "../data/eagle", pattern = "Passage.*html$", full.names = TRUE)
#' data.currentyear.eagle.list <- readsonarHTML(html.filenames)
#' data.currentyear.eagle.list <- extractsonarHTML(data.currentyear.eagle.list, passagetable = 'Daily Passage By Bank 2020 Eagle Summer Season')
#'
#' #the names of the tables as they came from the html file, plus the cleaned-up daily table and separate stats table:
#' names(data.currentyear.eagle.list[[1]])
#'
#' #grab the stats tables from each html
#' res <- lapply(data.currentyear.eagle.list, "[[","sonar.stats.wide")
#'
#' #convert list to data frame:
#' res <- do.call('rbind', res)
#'
#' res <- res[,c('date', "TOTAL.Total", "TOTAL.L90", "TOTAL.U90")]
#'
#' #get the daily counts from the last available html file:
#' res.daily <- data.currentyear.eagle.list[[length(data.currentyear.eagle.list)]]$sonar.current.year
#' merge(res.daily[,c("date", "TOTAL")], res, all = TRUE)
#'
#' #copy to clipboard each column for pasting to xls:
#' writeClipboard(as.character(res.daily$TOTAL))
#' writeClipboard(as.character(res.daily$TOTAL.L90))
#' writeClipboard(as.character(res.daily$TOTAL.U90))
#'
#'
#' }
readsonarHTML <- function(filenames){

  res <- lapply(filenames, function(filename.single){
    tables <- XML::readHTMLTable(filename.single, stringsAsFactors = FALSE)
    return(tables)
  })
  names(res) <- basename(filenames)

 return(res)

}#END readsonarHTML




readsonarHTML.archive <- function(filenames, passagetable=c("Daily Passage By Species", 'Daily Passage By Bank 2020 Eagle Summer Season')){
  #require(XML)
  passagetable <- match.arg(passagetable)

  res <- lapply(filenames, function(filename.single){
    tables <- XML::readHTMLTable(filename.single, stringsAsFactors = FALSE)
    sonar.current.year <- tables[[passagetable]]

    #remove commas in numbers:
    sonar.current.year[,-1] <- apply(sonar.current.year[,-1],2, function(x) as.integer(gsub(",", "", x)))

    rows.stats <- c("Total", "Var", "se", "cv(percent)", "L90", "U90")
    sonar.stats <- sonar.current.year[ sonar.current.year$Date %in% rows.stats,]
    colnames(sonar.stats)[1] <- "statistic"

    sonar.stats$statistic <- gsub(pattern = "\\(|\\)", replacement = "",sonar.stats$statistic)

    #remove the stats rows from the daily table:
    sonar.current.year <- sonar.current.year[! sonar.current.year$Date %in% rows.stats, ]

    sonar.current.year$od <- calcOrdinalDay(sonar.current.year$Date, format = "%m/%d/%Y")
    #sonar.current.year.total <- sum(sonar.current.year$ALLCHINOOK)

    sonar.current.year$date <- as.Date(calcDate(sonar.current.year$od))

    sonar.stats$date <- max(sonar.current.year$date)
    sonar.stats.wide <- reshape(sonar.stats, direction = 'wide', timevar = 'statistic', idvar = "date")
    row.names(sonar.stats) <- NULL
    row.names(sonar.stats.wide) <- NULL

    return(list(tables=tables, sonar.current.year=sonar.current.year, sonar.stats=sonar.stats, sonar.stats.wide=sonar.stats.wide))

  })
  names(res) <- filenames

  return(res)

}#END readsonarHTML.archive
